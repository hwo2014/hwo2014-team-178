package noobbot;

import java.util.ArrayList;

import com.google.gson.internal.LinkedTreeMap;

public class Throttler {
	
	private double angle;
	private double inPieceDistance;
	private int pieceIndex;
	private int startLaneIndex;
	private int endLaneIndex;
	private boolean changeDirection;
	private String changeDirectionWhere;
	public final double slowMultiplier = 0.7;
	public final double cautionLength = 10.0;
	public final int startingPieces = 5;
	private double fullThrottleDistance;
	private double[] angleSums;
	private boolean changedAlready;
	private ArrayList roadPieces;
	private ArrayList lanes;

	public Throttler() {
		this.angle = 0.0;
		this.inPieceDistance = 0.0;
		this.pieceIndex = 0;
		this.fullThrottleDistance = 0.0;
		this.changedAlready = false;
		this.changeDirection = false;
	}
	
	private double calculateSpeed (double dist1, double dist2, double radius) {
		if (dist2 > dist1) {
			return dist2-dist1;
		}
		else {
			LinkedTreeMap currentPiece = getPieceFromIndex(previousPieceIndex(this.pieceIndex));
			if (!(currentPiece.get("length") == null)) {
	    		return dist2+(double)currentPiece.get("length")-dist1;
	    	}
			else {
				if ((double)currentPiece.get("angle") > 0) {
					return 2.0*Math.PI*((double)currentPiece.get("radius")-radius)*Math.abs((double)currentPiece.get("angle"))/360.0;
				}
				else {
					return 2.0*Math.PI*((double)currentPiece.get("radius")+radius)*Math.abs((double)currentPiece.get("angle"))/360.0;
				}
			}
		}
	}
	
	public double calculateThrottle(double angle, double inPieceDistance) {
    	double nextTurnRadius = getNextTurnRadius();
    	double nextTurnAngle = getNextTurnAngle();
    	double angleDifference = Math.abs(angle) - Math.abs(this.angle);
    	if (Math.signum(angle) != Math.signum(this.angle)) {
    		angleDifference = Math.abs(angle-this.angle);
    	}
    	double oldAngle = this.angle;
    	this.angle = angle;
    	
//    	if(this.pieceIndex > startingPieces) {
//    		if(this.endLaneIndex > 0) {
//    			changeDirection("Left");
//    		}
//    	}
    	
    	if (distanceToNextTurnEvenIfOnTurn(this.pieceIndex, this.inPieceDistance) > distanceToNextSwitch(this.pieceIndex, this.inPieceDistance)) {
    		if (!changedAlready) {
    			if (getNextTurnAngleEvenOnTurn() > 0) {
    				changeDirection("Right");
    			}
    			else {
    				changeDirection("Left");
    			}
    			changedAlready = true;
    		}
    	}
    	
    	if (distanceToNextSwitch(this.pieceIndex, this.inPieceDistance) == 0) {
    		changedAlready = false;
    	}
    	
    	double distanceFromCenterOnCurLane = getDistanceFromCenter(this.endLaneIndex);
    	
    	if(nextTurnAngle > 0) {
    		distanceFromCenterOnCurLane = nextTurnRadius-distanceFromCenterOnCurLane;
    	}
    	else {
    		distanceFromCenterOnCurLane = nextTurnRadius+distanceFromCenterOnCurLane;
    	}
    	
    	System.out.println("angsp: " + calculateAngleSpeed(this.inPieceDistance,inPieceDistance, distanceFromCenterOnCurLane));
    	double angleSpeed = calculateAngleSpeed(this.inPieceDistance,inPieceDistance, distanceFromCenterOnCurLane);
    	double speed = calculateSpeed(this.inPieceDistance,inPieceDistance, distanceFromCenterOnCurLane);
    	System.out.println("speed: " + speed);
    	this.inPieceDistance = inPieceDistance;
    	
    	//this is to check for how long we are going to go with full speed
    	if (distanceToNextTurnWithBreaking(this.pieceIndex, this.inPieceDistance) == 0.0) {
    		this.fullThrottleDistance = 0.0;
    	}
    	else {
    		if (distanceToNextTurnWithBreaking(this.pieceIndex, this.inPieceDistance) > this.fullThrottleDistance) {
    			this.fullThrottleDistance += distanceToNextTurnWithBreaking(this.pieceIndex, this.inPieceDistance);
    		}
    	}
    	
    	
    	//lets try something
//    	if(Math.abs(this.angle) > 30) {
//    		return 0;
//    	}
			
    	
    	System.out.println(distanceToNextTurnWithBreaking(this.pieceIndex, this.inPieceDistance));
    	
    	//smaller is slower
    	double breakingMultiplier = calculateBreakingMultiplier(distanceToNextTurnWithBreaking(this.pieceIndex, this.inPieceDistance), this.fullThrottleDistance, distanceFromCenterOnCurLane, speed);
    	
    	if (distanceToNextTurnWithBreaking(this.pieceIndex, this.inPieceDistance) > 0.0) {
    		return breakingMultiplier;
    	}
    	
    	else {
    		if(getCurrentPiece().get("angle") != null && getPieceFromIndex(nextPieceIndex(this.pieceIndex)).get("angle") != null) {
    			if (Math.signum((double)getCurrentPiece().get("angle")) != Math.signum((double)getPieceFromIndex(nextPieceIndex(this.pieceIndex)).get("angle"))) {
    				if (Math.abs(this.angle-oldAngle) > 3.0) {
    					return 0.0;
    				}
    			}
    		}
    		if (angleSpeed > 0.105) {
    			return 0.0;
    		}
    		else if (angleDifference > 4.0) {
    			return 0.0;
    		}
    		else if (angleDifference > 1.0 && Math.abs(this.angle) > 10.0) {
    			return 1.0/(Math.pow(angleDifference, 2.0));
    		}
    		else {
    			return 1.0;
    		}
    	}
    }
	
	private double calculateAngleSpeed (double dist1, double dist2, double radius) {
		if (dist2>dist1) {
			if (getCurrentPiece().get("angle") == null) {
				return 0.0;
			}
			double pieceLength = 2.0*Math.PI*radius*Math.abs(((double)getCurrentPiece().get("angle")))/360.0;
			return (dist2-dist1)/pieceLength;
		}
		if (getCurrentPiece().get("angle") == null) {
			return 0.0;
		}
		if (getPieceFromIndex(previousPieceIndex(this.pieceIndex)).get("angle") == null) {
			return 0.0;
		}
		if (Math.signum((double)getCurrentPiece().get("angle")) != Math.signum((double)getNextPiece().get("angle"))) {
			return 10000.0;
		}
		
		return 0.0;
	}
	
	private double calculateBreakingMultiplier(double distance, double throttleDistance, double nextTurnRadius, double speed){
		double breakingDistanceMultiplier = 1.0;
		
//		if (nextTurnRadius>=110.0){
//			breakingDistanceMultiplier = 1.0;
//		}
//		else {
//			breakingDistanceMultiplier = 1.4*110.0/nextTurnRadius;
//		}
		
		
		if (distance < 100.0 && speed > 6.8) {
			return 0.0;
		}
		else {
			return 1.0;
		}
		
//		double slowSpeed = 0.2;
//		double fasterSpeed = 0.9;
//		
//		if(throttleDistance > 600.0 && distance/breakingDistanceMultiplier > 180.0){
//			return 1.0;
//		}
//		else if (throttleDistance > 600.0 && distance/breakingDistanceMultiplier > 80.0) {
//			return fasterSpeed/1.5;
//		}
//		else if (throttleDistance > 600.0){
//			return slowSpeed/2;
//		}
//		
//		if (distance/breakingDistanceMultiplier > 120.0){
//			return 1.0;
//		}
//		else if (distance/breakingDistanceMultiplier > 80.0) {
//			return fasterSpeed/2.0;
//		}
//		else {
//			return slowSpeed;
//		}
	}
	
	private double calculateThrottleFromRadius(double radius) {

//		if (0 < radius && radius <= 91) {
//			return 0.65;
//		}
//		if (91 < radius && radius <= 111) {
//			return 0.0075*radius-0.025;
//		}
//		if (radius > 111 && radius < 191) {
//			return 0.0025*radius + 0.5225;
//		}
//		return 1.0;
		
		if (radius == 90) {
			return 0.65;
		}
		if (radius == 110) {
			return 0.8;
		}
		if (radius>189) {
			return 1.0;
		}
		
		if (0 < radius && radius < 100) {
			//return 0.0175*x-1.275;
			return 0.017*radius-1.125;
		}
		else {
			//return 0.0035*x+0.265;
			return 0.00308333*radius+0.266667;
		}
	}
	
    private double smallestDistanceFromCenter() {
    	double currentSmallest = 10000.0;
    	for(int i = 0; i < lanes.size();++i){
    		LinkedTreeMap currentLane = (LinkedTreeMap)lanes.get(i);
    		if ((double)currentLane.get("distanceFromCenter") < currentSmallest) {
    			currentSmallest = (double)currentLane.get("distanceFromCenter");
    		}
    	}
    	return currentSmallest;
    }
    
    private double biggestDistanceFromCenter() {
    	double currentBiggest = -10000.0;
    	for(int i = 0; i < lanes.size();++i){
    		LinkedTreeMap currentLane = (LinkedTreeMap)lanes.get(i);
    		if ((double)currentLane.get("distanceFromCenter") > currentBiggest) {
    			currentBiggest = (double)currentLane.get("distanceFromCenter");
    		}
    	}
    	return currentBiggest;
    }
	
	private void changeDirection(String direction) {
		this.changeDirection = true;
    	this.changeDirectionWhere = direction;
	}
	
    //might be for some use, not currently used
    private void generateAngleSums() {
    	this.angleSums = new double[this.roadPieces.size()];
    	double currentSum = 0.0;
    	//let's assume there is at least one piece
    	for (int i = 0; i < angleSums.length;++i) {
    		LinkedTreeMap currentPiece = getPieceFromIndex(i);
    		if (currentPiece.get("length") == null) {
    			currentSum += (double) currentPiece.get("angle");
    			this.angleSums[i]=currentSum;
    		}
    		else {
    			this.angleSums[i]=0;
    		}
    	}
    }
    
    //index for the next piece in track
    private int nextPieceIndex (int currentIndex) {
    	if (currentIndex+1 == this.roadPieces.size()) {
    		return 0;
    	}
    	else {
    		return currentIndex+1;
    	}
    }
    
  //index for the previous piece in track
    private int previousPieceIndex (int currentIndex) {
    	if (currentIndex==0) {
    		return this.roadPieces.size()-1;
    	}
    	else {
    		return currentIndex-1;
    	}
    }
    
    //returns the distance to next turn piece
    private double distanceToNextTurn (int currentIndex, double inPieceDistance) {
    	LinkedTreeMap currentPiece = getPieceFromIndex(currentIndex);
    	if (currentPiece.get("length") == null) {
    		return 0.0;
    	}
    	int tempIndex = currentIndex;
    	double length = 0;
    	while (!(currentPiece.get("length") == null)) {
    		length += (double) currentPiece.get("length");
    		tempIndex = nextPieceIndex(tempIndex);
    		currentPiece = getPieceFromIndex(tempIndex);
    	}
    	return length-inPieceDistance;
    }
    
    //distance to next turn, even if car is on turn right now
    private double distanceToNextTurnEvenIfOnTurn (int currentIndex, double inPieceDistance) {
    	LinkedTreeMap currentPiece = getPieceFromIndex(currentIndex);
    	int tempIndex = currentIndex;
    	while (currentPiece.get("length") == null) {
    		tempIndex = nextPieceIndex(tempIndex);
    		currentPiece = getPieceFromIndex(tempIndex);
    		inPieceDistance = 0.0;
    	}
    	double length = 0;
    	while (!(currentPiece.get("length") == null)) {
    		length += (double) currentPiece.get("length");
    		tempIndex = nextPieceIndex(tempIndex);
    		currentPiece = getPieceFromIndex(tempIndex);
    	}
    	return length-inPieceDistance;
    }
    
    private double distanceToNextSwitch (int currentIndex, double inPieceDistance) {
    	LinkedTreeMap currentPiece = getPieceFromIndex(currentIndex);
    	if (!(currentPiece.get("switch") == null)) {
    		return 0.0;
    	}
    	int tempIndex = currentIndex;
    	double length = 0;
    	while (currentPiece.get("switch") == null) {
    		//length of current piece, even in turn
    		if (currentPiece.get("length") == null) {
    			length += 2.0*Math.PI*((double)currentPiece.get("radius"))*Math.abs((double)currentPiece.get("angle"))/360.0;
    		}
    		else {
        		length += (double) currentPiece.get("length");
    		}
    		tempIndex = nextPieceIndex(tempIndex);
    		currentPiece = getPieceFromIndex(tempIndex);
    	}
    	return length-inPieceDistance;
    }
    
    private double distanceToNextTurnWithBreaking (int currentIndex, double inPieceDistance) {
    	LinkedTreeMap currentPiece = getPieceFromIndex(currentIndex);
    	if (currentPiece.get("length") == null) {
    		if (calculateThrottleFromRadius((double)currentPiece.get("radius")) != 1.0) {
    			return 0.0;
    		}
		}
    	int tempIndex = currentIndex;
    	double length = 0;
    	boolean inLoop = true;
    	while (inLoop) {
    		//length of current piece, even in turn
    		if (currentPiece.get("length") == null) {
    			length += 2.0*Math.PI*((double)currentPiece.get("radius"))*Math.abs((double)currentPiece.get("angle"))/360.0;
    		}
    		else {
        		length += (double) currentPiece.get("length");
    		}
    		tempIndex = nextPieceIndex(tempIndex);
    		currentPiece = getPieceFromIndex(tempIndex);
    		if (currentPiece.get("length") == null) {
    			if (calculateThrottleFromRadius((double)currentPiece.get("radius")) != 1.0) {
    				inLoop = false;
    			}
    		}
    	}
    	return length-inPieceDistance;
    }
    
    //returns the index of next turn
    private int indexOfNextTurn (int currentIndex) {
    	LinkedTreeMap currentPiece = getPieceFromIndex(currentIndex);
    	if (currentPiece.get("length") == null) {
    		return currentIndex;
    	}
    	int tempIndex = currentIndex;
    	while (!(currentPiece.get("length") == null)) {
    		tempIndex = nextPieceIndex(tempIndex);
    		currentPiece = getPieceFromIndex(tempIndex);
    	}
    	return tempIndex;
    }
    
    //returns the index of next turn even if on turn
    private int indexOfNextTurnEvenOnTurn (int currentIndex) {
    	LinkedTreeMap currentPiece = getPieceFromIndex(currentIndex);
    	int tempIndex = currentIndex;
    	while (currentPiece.get("length") == null) {
    		tempIndex = nextPieceIndex(tempIndex);
    		currentPiece = getPieceFromIndex(tempIndex);
    	}
    	while (!(currentPiece.get("length") == null)) {
    		tempIndex = nextPieceIndex(tempIndex);
    		currentPiece = getPieceFromIndex(tempIndex);
    	}
    	return tempIndex;
    }
    
    //calculating the percent that has been travelled trough piece, returns -1 in turns
    private double distancePercent(double inPieceDistance, int currentIndex) {
    	if(((LinkedTreeMap) this.roadPieces.get((int)currentIndex)).get("length") == null) {
    		return 0.0;
    	}
    	return inPieceDistance/(double) (((LinkedTreeMap) this.roadPieces.get((int)currentIndex))).get("length");
    }
    
    public boolean getChangeDirection() {
    	if(this.changeDirection == true){
    		this.changeDirection = !this.changeDirection;
    		return !this.changeDirection;
    	}
    	return this.changeDirection;
    }
    
    public String changeDirectionWhere() {
    	return this.changeDirectionWhere;
    }
    
    private double getCorrectedRadius(int index, double radius) {
    	if(getTurnAngle(index) > 0) {
    		return radius-getDistanceFromCenter(index);
    	}
    	else {
    		return radius+getDistanceFromCenter(index);
    	}
    }
    
    private double getDistanceFromCenter(int lane) {
    	for(int i = 0; i < lanes.size();++i){
    		LinkedTreeMap currentLane = (LinkedTreeMap)lanes.get(i);
    		if (((Double)currentLane.get("index")).intValue() == lane) {
    			return (double)currentLane.get("distanceFromCenter");
    		}
    	}
    	return -1.0;
    }
    
    private int getNumberOfLanes() {
    	return lanes.size();
    }
    
	private LinkedTreeMap getCurrentPiece() {
		return (LinkedTreeMap) (this.roadPieces.get(this.pieceIndex));
	}
	
	private LinkedTreeMap getPieceFromIndex(int index) {
		return (LinkedTreeMap) (this.roadPieces.get(index));
	}
	
	private LinkedTreeMap getNextPiece() {
		return (LinkedTreeMap) (this.roadPieces.get(indexOfNextTurn(this.pieceIndex)));
	}
	
	private double getNextTurnRadius() {
		return (double)((LinkedTreeMap) this.roadPieces.get(indexOfNextTurn(this.pieceIndex))).get("radius");
	}
	
	private double getNextTurnAngle() {
		return (double)((LinkedTreeMap) this.roadPieces.get(indexOfNextTurn(this.pieceIndex))).get("angle");
	}
	
	private double getNextTurnAngleEvenOnTurn () {
		return (double)((LinkedTreeMap) this.roadPieces.get(indexOfNextTurnEvenOnTurn(this.pieceIndex))).get("angle");
	}
	
	private double getTurnAngle(int index) {
		return (double)((LinkedTreeMap) this.roadPieces.get(index)).get("angle");
	}
	
	private double getTurnRadius(int index) {
		if (((LinkedTreeMap)(this.roadPieces.get(index))).get("angle") == null) {
			return 0.0;
		}
		return (double)((LinkedTreeMap) this.roadPieces.get(index)).get("angle");
	}

	public double getAngle() {
		return angle;
	}

	public double getInPieceDistance() {
		return inPieceDistance;
	}

	public int getPieceIndex() {
		return pieceIndex;
	}
	
	public void setPieceInfo(int pieceIndex){
		this.pieceIndex = pieceIndex;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public void setInPieceDistance(double inPieceDistance) {
		this.inPieceDistance = inPieceDistance;
	}

	public void setPieceIndex(int pieceIndex) {
		this.pieceIndex = pieceIndex;
	}

	public ArrayList getRoadPieces() {
		return roadPieces;
	}

	public void setRoadPieces(ArrayList roadPieces) {
		this.roadPieces = roadPieces;
	}

	public ArrayList getLanes() {
		return lanes;
	}

	public void setLanes(ArrayList lanes) {
		this.lanes = lanes;
	}

	public int getStartLaneIndex() {
		return startLaneIndex;
	}

	public int getEndLaneIndex() {
		return endLaneIndex;
	}

	public void setStartLaneIndex(int startLaneIndex) {
		this.startLaneIndex = startLaneIndex;
	}

	public void setEndLaneIndex(int endLaneIndex) {
		this.endLaneIndex = endLaneIndex;
	}

}