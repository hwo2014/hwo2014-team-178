package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class Main {
	
	Throttler throttler;
	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    @SuppressWarnings("rawtypes")
	public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	//stuff to read data from server msg
            	ArrayList tempRaceData = (ArrayList) msgFromServer.data;
            	LinkedTreeMap raceData = (LinkedTreeMap) tempRaceData.get(0);
            	LinkedTreeMap pieceData = (LinkedTreeMap) raceData.get("piecePosition");
            	LinkedTreeMap laneData = (LinkedTreeMap) pieceData.get("lane");
            	System.out.println(msgFromServer.data);
            	System.out.println();
            	throttler.setPieceInfo(((Double) pieceData.get("pieceIndex")).intValue());
            	int startLaneIndex = ((Double)laneData.get("startLaneIndex")).intValue();
            	int endLaneIndex = ((Double)laneData.get("endLaneIndex")).intValue();
            	throttler.setStartLaneIndex(startLaneIndex);
            	throttler.setEndLaneIndex(endLaneIndex);
            	double throttle = throttler.calculateThrottle((double) raceData.get("angle"), (double) pieceData.get("inPieceDistance"));
            	System.out.println(throttle);
            	if(throttler.getChangeDirection()) {
            		send(new Switch(throttler.changeDirectionWhere()));
            	}
            	else {
                	//sending throttle
                    send(new Throttle(throttle));
            	}
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	//getting the information from track pieces
            	throttler = new Throttler();
            	LinkedTreeMap gameInitData = (LinkedTreeMap) msgFromServer.data;
            	LinkedTreeMap race = (LinkedTreeMap) gameInitData.get("race");
            	LinkedTreeMap track = (LinkedTreeMap) race.get("track");
            	throttler.setRoadPieces((ArrayList) (track.get("pieces")));
            	throttler.setLanes((ArrayList) (track.get("lanes")));
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
    
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Switch extends SendMsg {
	private String direction;
	
	public Switch (String direction) {
		this.direction = direction;
	}
	
	@Override
	protected Object msgData() {
		return direction;
	}
	
	@Override
	protected String msgType() {
		return "switchLane";
	}
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}